// Import required libraries
#include <Arduino.h>
#include <WiFi.h>
#include "WebSocketClient.h"

// Define variables
#define LED 4

// WiFi parameters
const char *ssid = "RIT-Legacy";
IPAddress hostip(129, 21, 79, 39);
const uint16_t port = 8080;
WebSocketClient webSocketClient;
WiFiClient client;

char path[] = "/";
char host[] = "129.21.79.39";

void setup()
{
    // Set pinModes
    pinMode(LED, OUTPUT);

    // Start Serial
    Serial.begin(115200);

    WiFi.disconnect(true); //disconnect form wifi to set new wifi connection
    WiFi.mode(WIFI_STA);

    // Connect to WiFi
    WiFi.begin(ssid);
    WiFi.setHostname("ESP32Name");
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi connected");

    // Print the IP address
    Serial.println(WiFi.localIP());

    if (!client.connect(hostip, port))
    { // HTTP connection on port 8080
        Serial.println("Connection lost! - Failed response");
        return;
    }

    webSocketClient.path = path;
    webSocketClient.host = host;
    if (webSocketClient.handshake(client))
    {
        Serial.println("Handshake successful");
    }
    else
    {
        Serial.println("Handshake failed.");
    }
}

void loop()
{
    String data;

    if (client.connected())
    {

        webSocketClient.sendData("Oh hello there!");

        webSocketClient.getData(data);
        if (data.length() > 0)
        {
            Serial.print("Received data: ");
            Serial.println(data);
        }
    }
    else
    {
        do {
        Serial.println("Client disconnected.");
        Serial.println("Attempting to reconnect...");
        client.stop();
        delay(500);
        } while (!client.connect(hostip, port));
    }

    delay(1000);
}